import Vue from 'vue'
import Router from 'vue-router';

Vue.use(Router);

const Login = resolve => {
  require.ensure(['../view/login'], () => {
    resolve(require('../view/login'));
  });
}

const LoginRoute = [{
  path: '/login',
  name: 'login',
  component: Login
}]

const Main = resolve => {
  require.ensure(['../view/main'], () => {
    resolve(require('../view/main'));
  });
}

const Book = resolve => {
  require.ensure(['../view/book'], () => {
    resolve(require('../view/book'));
  });
}

const Search = resolve => {
  require.ensure(['../view/search'], () => {
    resolve(require('../view/search'));
  });
}

const BookHistory = resolve => {
  require.ensure(['../view/bookhistory'], () => {
    resolve(require('../view/bookhistory'));
  });
}

const IndexRoute = [{
  path: '/',
  name: 'main',
  redirect: 'book',
  component: Main,
  children:[{
    path: 'book',
    name: 'book',
    component: Book
  },{
    path: 'search',
    name: 'search',
    component: Search
  },{
    path: 'bookhistory',
    name: 'bookhistory',
    component: BookHistory
  }]
}]

const router = new Router({
  routes: [
    ...LoginRoute,
    ...IndexRoute
  ]
})

export default router